# README #

This is the repository for classifier used on 2nd stage of the VPP

### What is this repository for? ###

* Quick summary: b2bot-tech/Dataset/b2botclassifier
* Version 1.0.0


### How do I get set up? ###

* Summary of set up:

        cfg directory includes: 
            weight of inception5h pretrained for 1000 objects on imagenet under protobuf format
    
        data directory includes: 
            detail object name list in french
            detail object name list in english

* Configuration
* Dependencies

    Pretrained data: 
    
        https://storage.googleapis.com/download.tensorflow.org/models/inception5h.zip
    
    untested tensorflow models are in: 
    
        https://github.com/tensorflow/models/tree/master/research/slim#pre-trained-models 

    tensorflow:
    
        https://github.com/tensorflow

* Database configuration
* How to run tests
* Deployment instructions
 
    Usage withing the VPP engine from: 
    https://github.com/ezdayo/vpp

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin: francois.badaud@gmail.com
* Other community or team contact: ezdayo@gmail.com